php-vlucas-phpdotenv (5.6.1-2) unstable; urgency=medium

  * Team upload
  * Modernize PHPUnit syntax
  * Update Standards-Version to 4.7.2

 -- David Prévot <taffit@debian.org>  Sun, 02 Mar 2025 18:48:08 +0100

php-vlucas-phpdotenv (5.6.1-1) unstable; urgency=medium

  * Team upload
  * Drop -v flag from phpunit call (Closes: #1092124)
  * Revert "Force system dependencies loading"
  * New upstream version 5.6.1

 -- David Prévot <taffit@debian.org>  Sat, 25 Jan 2025 11:32:42 +0100

php-vlucas-phpdotenv (5.6.0-1) unstable; urgency=medium

  * New upstream version 5.6.0
  * Fix PHPUnit 11 compatibility (Closes: #1070609)
  * Fix pkg-php-tools 1.45 compatibility (Closes: #1074208)
  * Update Standards-Version
  * Remove <!nocheck> on PHP library dependencies
  * Use my debian.org email in Uploaders

 -- Robin Gustafsson <rgson@debian.org>  Sat, 06 Jul 2024 17:17:07 +0200

php-vlucas-phpdotenv (5.4.1-2) unstable; urgency=medium

  * Team upload
  * Force system dependencies loading

 -- David Prévot <taffit@debian.org>  Fri, 08 Mar 2024 20:09:19 +0100

php-vlucas-phpdotenv (5.4.1-1) unstable; urgency=medium

  [ Robin Gustafsson ]
  * New upstream version 5.4.1
  * Set upstream metadata fields: Security-Contact.
  * Bump Standards-Version
  * Replace git attributes with uscan's gitexport=all
  * Rename main branch to debian/latest (DEP-14)
  * Mark test dependencies with build profile spec
  * Avoid upstream's "make install" command
  * Build-Depend on dh-sequence-phpcomposer
  * Install a pkg-php-tools autoloader file
  * Generate phpab template with phpabtpl

  [ Katharina Drexel ]
  * Bump new standards version.
  * Removing deprecated patches.
  * Adding build dependeny for php-graham-campbell-result-type.

 -- Robin Gustafsson <robin@rgson.se>  Sat, 23 Jul 2022 17:54:39 +0200

php-vlucas-phpdotenv (3.6.7-2) unstable; urgency=medium

  * Source only upload for migration to testing
  * Remove Salsa CI config

 -- Robin Gustafsson <robin@rgson.se>  Sun, 06 Dec 2020 16:47:52 +0100

php-vlucas-phpdotenv (3.6.7-1) unstable; urgency=medium

  * New upstream release
  * Add Upstream-Name and Upstream-Contact

 -- Robin Gustafsson <robin@rgson.se>  Thu, 27 Aug 2020 20:05:34 +0200

php-vlucas-phpdotenv (3.6.4-1) unstable; urgency=medium

  * Initial release (Closes: #951163)

 -- Robin Gustafsson <robin@rgson.se>  Tue, 12 May 2020 21:09:10 +0200
